#include "turtle.h"
#include "math_fun.h"
#include <cmath>
#include "lcd_lib/LCD_ILI9325.h"

Turtle::Turtle(double angle, double xpos, double ypos): _angle(angle), _xpos(xpos), _ypos(ypos),
														_is_pen_down(true), _printer(LCD_MAX_X, LCD_MAX_Y)	{}
																											 
void Turtle::setxy(double x, double y)
{
	if(_is_pen_down){
		_printer.line(static_cast<int>(round(_xpos)), static_cast<int>(round(_ypos)), static_cast<int>(round(x)), static_cast<int>(round(y)));
	}
	#if DEBUG
	_printer.print_int(static_cast<int>(_xpos), 200, 240);
	_printer.print_int(static_cast<int>(_ypos), 200, 270);
	#endif
	_xpos = x;
	_ypos = y;
	#if DEBUG
	_printer.print_int(static_cast<int>(_xpos), 180, 240);
	_printer.print_int(static_cast<int>(_ypos), 180, 270);
	#endif
}

void Turtle::setx(double x)
{
	setxy(x, _ypos);
}

void Turtle::sety(double y)
{
	setxy(_xpos, y);
}

void Turtle::seth(double angle)
{
	/*if(angle < 0)
	{
		double x360 = abs(angle) / 360 + 1;
		angle = x360 * 360 - angle;
	}
	
	_angle = angle % 360;*/
	_angle = angle;
	
	#if DEBUG
	_printer.print_int(_angle, 100, 20);
	#endif
}

void Turtle::left(double angle)
{
	seth(_angle - angle);
}

void Turtle::right(double angle)
{
	seth(_angle + angle);
}

void Turtle::forward(double length)
{
	double x = _xpos + length * cos(to_radians(_angle));
	double y = _ypos + length * sin(to_radians(_angle));
	
	#if DEBUG
	_printer.print_int(static_cast<int>(cos(_angle)*1000.0), 160, 230);	
	_printer.print_int(static_cast<int>(sin(_angle)*1000.0), 160, 270);	
	#endif
	setxy(x, y);
}

void Turtle::getxy()
{
	_printer.print_string("x", 1, 235, 26);
	_printer.print_int(static_cast<int>(_xpos), 220, 260);
	_printer.print_string("y", 1, 235, 290);
	_printer.print_int(static_cast<int>(_ypos), 220, 290);
}

void Turtle::pen_up() 
{ 
	#if DEBUG
	_printer.print_string("up", 2, 200, 20);
	#endif
	_is_pen_down = false; 
}
void Turtle::pen_down() 
{ 
	#if DEBUG
	_printer.print_string("up", 2, 200, 20);
	#endif
	_is_pen_down = true; 
}
