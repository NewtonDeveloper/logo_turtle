#include "interpreter.h"

#include <iostream>

using namespace std;

int main()
{

    while(true)
    {
    string input{ "(13-4)-(2+1)" };
    //string input;
    getline(cin,input);
    cout << input << endl;
    vector<Token> tokens = lex(input);
    for (unsigned i = 0; i < tokens.size(); i++) 
    {
        cout<< tokens[i].text << endl;    
    }
    Element* parsed = parse(tokens);
    //printf("%s = %d\n", input.c_str(), parsed->eval());   
    cout << input << " = " << parsed->eval() << endl;
    }
    return 0;
}
