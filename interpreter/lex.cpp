#include <cctype>
#include <vector>

#include <string>
#include "Token.h"


using namespace std;

/*
    Funkcja przekształcająca wejściowy string na sekwencję Tokenów
*/
vector<Token> lex(const string &input) {
    vector<Token> result;

    for (size_t i = 0; i < input.size(); ++i) {

       if(isalpha(input[i]))
       {    
            string buffer;
            buffer += input[i];
            for (size_t j = i + 1; j < input.size(); ++j)
            {
                 if (isalpha(input[j])) {

                    buffer += input[j];
                    ++i;
                  } 
                  else 
                      break;
            }
            if(buffer == "forward")
            {
            result.push_back(Token(Token::forward, buffer));
            }

            if(buffer == "right")
            {
            result.push_back(Token(Token::right, buffer));
            }

            if(buffer == "back")
            {
            result.push_back(Token(Token::back, buffer));
            }

            if(buffer == "left")
            {
            result.push_back(Token(Token::left, buffer));
            }

            if(buffer == "penup")
            {
            result.push_back(Token(Token::penup, buffer));
            }

            if(buffer == "pendown")
            {
            result.push_back(Token(Token::pendown, buffer));
            }

            if(buffer == "getxy")
            {
            result.push_back(Token(Token::getxy, buffer));
            }

            if(buffer == "setxy")
            {
            result.push_back(Token(Token::setxy, buffer));
            }

            if(buffer == "repeat")
            {
            result.push_back(Token(Token::repeat, buffer));
            }
 
			if(buffer == "clearscreen")
            {
            result.push_back(Token(Token::clearscreen, buffer));
            }
            i=i+1; // szukam int    forward <int>

        }
      
       if(isdigit(input[i]))
       {    
        string buffer;
        buffer += input[i];
        for (size_t j = i + 1; j < input.size(); ++j) {
            /*
                Parsuje liczbe całkowitą interger 
                Za pomocą bufora budujemy liczbę, dopóki kolejno występują
                cyfry.
            */
            if (isdigit(input[j])) 
            {
                buffer += input[j];
                ++i;
            }
            else
                break;
          }
       
        result.push_back(Token(Token::integer, buffer));
        i += 1;  // ustawian nastepny znak do rozpatrzenia
        }

        switch (input[i]) {
        case '+':
            result.push_back(Token(Token::plus, "+"));
            break;
        case '-':
            result.push_back(Token(Token::minus, "-"));
            break;
        case '*':
            result.push_back(Token(Token::multiply, "*"));
            break;
       case '/':
            result.push_back(Token(Token::divide, "/"));
            break;
        case '(':
            result.push_back(Token(Token::lparen, "("));
            break;
        case ')':
            result.push_back(Token(Token::rparen, ")"));
            break;
        case '[':
            result.push_back(Token(Token::lsqareparen, "["));
            break;
        case ']':
            result.push_back(Token(Token::rsqareparen, "]"));
            break;
        case ' ':
            result.push_back(Token(Token::space, " "));
            break;
       
        /*
            Default - napotkaliśmy nie znany znak
        */
        default:
        break;
            
        }
    }

    return result;
}

