#include <memory>
#include <string>
#include "interpreter.h"
#include <cstdlib>
using namespace std;

#include "../turtle.h"

extern Turtle turtle;


int stoi(string str)
{
	return atoi(str.c_str());
}



/*
    W naszym problemie wszystkie operacje są binarne - dodajemy coś do czegoś.
*/

    /*
        Operacja ma element po lewej i po prawej stronie znaku operacji
    */

int BinaryOperation::eval() const {
	
	int lvalue = 0;
	int rvalue = 0;

	if( lhs != nullptr)
	{
		lvalue = lhs->eval();
		delete lhs;
	}

	if( rhs != nullptr)
	{
		rvalue = rhs->eval();
		delete rhs;
	}
	
        if (type == addition)
            return lvalue + rvalue;
        if (type == subtraction)
            return lvalue - rvalue;
        if (type == multiplication)
            return lvalue * rvalue;
        if (type == divide)
            return lvalue / rvalue;
        if (type == no_operation)
            return lvalue;
        if (type == nothing)
            return 0;
        if (type == forward_command)
        {
            //Test::forward(rvalue);
			turtle.forward(rvalue);
            return rvalue;
        }
        if (type == left_command)
        {
            //Test::left(rvalue);
			turtle.left(rvalue);
            return rvalue;
        }
        if (type == right_command)
        {
            //Test::right(rvalue);
			turtle.right(rvalue);
            return rvalue;
        }

        if (type == back_command)
        {
            //Test::back(rvalue);
			turtle.forward(-rvalue);   // TODO zaimplementować w żółwiu
            return rvalue;
        }

 
        if (type == penup_command)
        {
            //Test::penup();
			turtle.pen_up();
            return 0;
        }

  
        if (type == pendown_command)
        {
            //Test::pendown();
			turtle.pen_down();
            return 0;
        }
 
        if (type == setxy_command)
        {
            //Test::setxy(lvalue,rvalue);
			turtle.setxy(lvalue, rvalue);
            return 0;
        }

        if (type == getxy_command)
        {
            //Test::getxy();
			turtle.getxy();
            return 0;
        }

        if (type == clearscreen_command)
        {
            //Test::clearscreen();
			turtle._printer.clear_screen();
            return 0;
        }

        return 0;
   }




unsigned find_next_command(int j, const vector<Token>& tokens)
{
    unsigned k;
    for (k = j; k < tokens.size(); ++k)
    {
        if (tokens[k].type == Token::left
            || tokens[k].type == Token::right
            || tokens[k].type == Token::forward
            || tokens[k].type == Token::back
            || tokens[k].type == Token::penup
            || tokens[k].type == Token::pendown
            || tokens[k].type == Token::getxy
            || tokens[k].type == Token::setxy
            || tokens[k].type == Token::clearscreen
           )
           break;
    }
    return k;
}



/*
    Parsowanie: zamiana Tokenów na drzewo binarne Operacji
*/

Element * parse(const vector<Token> &tokens) {
    /*
        Rezultatem Parsowania jest pojedyncza operacja, którą można ewaluować
    */
    BinaryOperation *result(new BinaryOperation);

    result->type = BinaryOperation::nothing;
    /*
        W korzeniu drzewa mamy Binary operation, które ma 2 operandy: lewy
        i prawy. Jeśli jednak natkniemy się na liczbę - musimy wiedzieć, czy
        stoi po prawej czy po lewej stronie znaku operacji.
    */
    bool have_lhs = false;

    /*
        iterujemy po każdym Tokenie
    */
    for (unsigned i = 0; i < tokens.size(); i++) { // można użyć iteratorów
        const Token& token = tokens[i];
        switch (token.type) {

        /* Jeśli integer - przerabiamy go na liczbę */
        case Token::integer: {
            int value = stoi(token.text); // zamiana na int przy użyciu
                                          // std::stoi(...)
            Integer *integer(new Integer(value));
            if (!have_lhs) {
                result->lhs = integer;
                result->type = BinaryOperation::no_operation;
                have_lhs = true;
            } else
                result->rhs = integer;
        }
        break;
        case Token::plus:
            result->type = BinaryOperation::addition;
            break;
        case Token::minus:
            result->type = BinaryOperation::subtraction;
            break;
        case Token::multiply:
            result->type = BinaryOperation::multiplication;
            break;
        case Token::divide:
            result->type = BinaryOperation::divide;
            break;
        /*
            Kiedy natrafimy na nawias otwierający - szukamy nawiasu zamykającego
        */
        case Token::lparen: {
            unsigned j;
            for (j = i; j < tokens.size(); ++j)
                if (tokens[j].type == Token::rparen)
                    break; // nawias znaleziony

            /*
                Tworzymy podwyrażenie (pomiędzy znalezionymi nawiasami)...
            */
            vector<Token> subexpression(&tokens[i + 1], &tokens[j]);

            /*
                ...które jest rekurencyjnie parsowane, aż nie będzie nawiasów.
            */
            Element * element = parse(subexpression);
            if (!have_lhs) {
                result->lhs = element;
                have_lhs = true;
            } else
                result->rhs = element;
            i = j; // przesuwamy się dalej
        }
        break;
        /*
            Nie rozważamy wystąpienia nawiasów zamykających przed otwierającymi,
            więc nie ma po co rozważać wystąpienia takiego Tokena.
        */

        case Token::forward: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j,tokens); // szukam nastpnej komendy w lini 
                        vector<Token> subexpression(&tokens[j], &tokens[k]);

            Element *element = parse(subexpression);
            result->rhs = new Integer(element->eval());
	    delete element;
            result->type = BinaryOperation::forward_command;    

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;
        }
        break;

        case Token::left: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> subexpression(&tokens[j], &tokens[k]);
            Element *element = parse(subexpression);
            result->rhs = new Integer(element->eval());
	    delete element;
            result->type = BinaryOperation::left_command;    

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;
 
        }
        break;

        case Token::right: {


            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> subexpression(&tokens[j], &tokens[k]);

            Element *element = parse(subexpression);
            result->rhs = new Integer(element->eval());
	    delete element;
            result->type = BinaryOperation::right_command;             
            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;


        case Token::back: {


            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> subexpression(&tokens[j], &tokens[k]);

            Element *element = parse(subexpression);
            result->rhs = new Integer(element->eval());
	    delete element;
            result->type = BinaryOperation::back_command;             
            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;
    

      case Token::penup: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::penup_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;
    
       

     case Token::pendown: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::pendown_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;
    
     case Token::getxy: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::getxy_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;

     case Token::setxy: {
            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::setxy_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            unsigned s; //setxy x y szukam spacji pomiedzy x i y
            for(s=j+1; s < k; ++s)
            {
             if( tokens[s].type == Token::space)
                break;
            }

            vector<Token> subexpression_x(&tokens[j], &tokens[s]);
            vector<Token> subexpression_y(&tokens[s+1], &tokens[k]);

            Element *x_value = parse(subexpression_x);
            Element *y_value = parse(subexpression_y);
	    delete x_value;
	    delete y_value;
            result->lhs = new Integer(x_value->eval());
            result->rhs = new Integer(y_value->eval());
            result->type = BinaryOperation::setxy_command;    

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;

      case Token::clearscreen: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::clearscreen_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned k = find_next_command(j, tokens); // szukam nastpnej komendy w lini 

            vector<Token> next_expression(&tokens[k], &tokens[tokens.size()]);
			/**/
			//result->type = BinaryOperation::clearscreen_command;
			/**/
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;

      case Token::repeat: {

            unsigned j= i + 2;  // omijam pierwszą spacje za komendą
            result->type = BinaryOperation::clearscreen_command;
            if( tokens[i+1].type != Token::space)
                break;

            unsigned p;
            for (p = j; p < tokens.size(); ++p)
                if (tokens[p].type == Token::lsqareparen)
                    break; // nawias znaleziony

            unsigned k;
            for (k = p; k < tokens.size(); ++k)
                if (tokens[k].type == Token::rsqareparen)
                    break; // nawias znaleziony

                        vector<Token> subexpression(&tokens[j], &tokens[p]);

            Element * element = parse(subexpression);
            int repeat_value = element->eval();
	    delete element;
            result->rhs = new Integer(repeat_value);
            result->type = BinaryOperation::nothing;    

            for(int r = 1; r <= repeat_value; ++r)
            {
                vector<Token> repeatexpression(&tokens[p+1], &tokens[k]);
                Element *element = parse(repeatexpression);
		element->eval();
		delete element;
            }

            vector<Token> next_expression(&tokens[k+1], &tokens[tokens.size()]);
 
            Element * next_element = parse(next_expression);
            next_element->eval();
	    delete next_element;

            i=tokens.size(); // warunek zakonczenia
            //i=k-1;

        }
        break;




        default:
            break;
        }
    }

    return result;
}
