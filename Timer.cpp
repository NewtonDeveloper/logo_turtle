#include "Timer.h"


volatile uint32_t msTicks = 0;   


void SysTick_Handler(void)  {   
	msTicks++;
}

void wait_ms(int ms) {
	msTicks = 0;

	while (msTicks < ms) {}
}
