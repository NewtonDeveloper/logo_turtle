#ifndef TIMER_H
#define TIMER_H

#include "lcd_lib/Open1768_LCD.h"

                                                                  
void SysTick_Handler(void);

void wait_ms(int ms);

#endif