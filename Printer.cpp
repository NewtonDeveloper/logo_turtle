#include "lcd_lib/Open1768_LCD.h"
#include "lcd_lib/LCD_ILI9325.h"
#include "lcd_lib/asciiLib.h"
#include "tp_lib/TP_Open1768.h"
#include "Printer.h"
#include "math_fun.h"


Printer::Printer(const int &max_x, const int &max_y): _max_x(max_x), _max_y(max_y),
																					_max_xy(max_x * max_y), _color(LCDBlack) 
{
	lcdConfiguration();
	init_ILI9325();
}

void Printer::print_letter(char letter, int y_pos, int x_pos)
{
	int j = 0;
	int i = 0;
	unsigned char pBuffer[letter_size];
	unsigned char mask = 128;

	GetASCIICode(1, pBuffer, letter);
	lcdWriteReg(ADRX_RAM, x_pos);
	lcdWriteReg(ADRY_RAM, y_pos);
	for( i = 0; i < letter_size; ++i)
	{
		for( j = 0; j < 8; ++j)
		{
			lcdWriteReg(ADRX_RAM, x_pos - i);
			lcdWriteReg(ADRY_RAM, y_pos + j);
			if (pBuffer[i] & mask)
			{		
					lcdWriteReg(DATA_RAM, LCDMagenta);
			}
			else {
					lcdWriteReg(DATA_RAM, LCDWhite);
			}
				
			mask = mask >> 1;
		}
			mask = 128;
	}
}

void Printer::print_string(const char* string, int string_size, int x_pos, int y_pos)
{
	int i;
	
	for(i=0; i <  string_size; ++i)
	{
		print_letter(string[i], y_pos, x_pos);
		y_pos += 8;
	}
}

void Printer::print_int(int value, int x_pos, int y_pos) 
{
	char number [20] = {0};
	
	sprintf(number, "%d", value);
	
	print_string(number, strlen(number), x_pos, y_pos);
}

void Printer::clear_screen()
{
	int i=0;
	
	lcdWriteReg(ADRX_RAM, 0);
	lcdWriteReg(ADRY_RAM, 0);
	
	while(i < _max_xy){
		lcdWriteReg(DATA_RAM, LCDWhite);
		++i;
	}
}

void Printer::point(const int &x, const int &y)
{
	lcdWriteReg(ADRX_RAM, x);
	lcdWriteReg(ADRY_RAM, y);
	lcdWriteReg(DATA_RAM, _color);
}

void Printer::line(int start_x, int start_y, int end_x, int end_y)
{
	int dx = abs(end_x-start_x), sx = start_x<end_x ? 1 : -1;
	int dy = abs(end_y-start_y), sy = start_y<end_y ? 1 : -1; 
	int err = (dx>dy ? dx : -dy)/2, e2;
	
	while(true){
		point(start_x,start_y);
		if (start_x==end_x && start_y==end_y) break;
		e2 = err;
		if (e2 >-dx) { err -= dy; start_x += sx; }
		if (e2 < dy) { err += dx; start_y += sy; }
	}
}