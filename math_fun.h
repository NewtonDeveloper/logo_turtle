#ifndef MATH_FUN_H
#define MATH_FUN_H

#include <math.h>


inline int abs(int a)
{
		if(a < 0)
			a = -a;
		return a;
}

inline double to_radians(double angle)
{
	return angle / 180.0 * 3.14159265359;
}

inline double round(double val)
{
	double temp = floor(val);
	if (fabs(val - temp) > 0.5)
		return temp + 1.0;
	return temp;
}
/*
inline double cos(int a)
{
	return cos(to_radians(static_cast<double>(a)));
}

inline double sin(int a)
{
	return sin(to_radians(static_cast<double>(a)));
}
*/

#endif
