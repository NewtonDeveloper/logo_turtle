#ifndef TEST_H
#define TEST_H

#include <iostream>
using namespace std;

struct Test
{
	static void left(int arg_val)
	{
		cerr << "left(" << arg_val << ")\n"; 
	}

	static  void right(int arg_val)
	{
		cerr << "right(" << arg_val << ")\n"; 
	}


	static void forward(int arg_val)
	{
		cerr << "forward(" << arg_val << ")\n"; 
	}

 	static void back(int arg_val)
	{
		cerr << "back(" << arg_val << ")\n"; 
	}

 	static void penup()
	{
		cerr << "penup()\n"; 
	}

	static void pendown()
	{
		cerr << "pendown()\n"; 
	}

	static void getxy()
	{
		cerr << "getxy()\n"; 
	}

	static void setxy(int arg_x, int arg_y)
	{
		cerr << "setxy(" << arg_x << ", " << arg_y << ")\n"; 
	}

	static void clearscreen()
	{
		cerr << "clearscreen()\n"; 
	}


};


#endif

