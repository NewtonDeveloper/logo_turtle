#ifndef TURTLE_H
#define TURTLE_H

#include "Printer.h"

class Turtle
{
public:

	Turtle(double angle, double xpos, double ypos);

	void setxy(double x, double y);
	void setx(double x);
	void sety(double y);

	void getxy();

	void seth(double angle);
	void left(double angle);
	void right(double angle);

	void forward(double lenght);

	void pen_up();
	void pen_down();

	Printer _printer;
	bool _is_pen_down;
	double _angle;
	double _xpos, _ypos;

};

#endif
