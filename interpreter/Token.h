#ifndef TOKEN_H
#define TOKEN_H


using namespace std;

struct Token
{
    enum Type { integer, plus, minus, lparen, rparen, space
                ,forward, right, left, back, penup, pendown,
    			getxy, setxy, clearscreen, lsqareparen, rsqareparen
				, repeat, multiply, divide} type;
    string text;

    explicit Token(Type type, const string& text)
    : type(type), text(text) {}

    string get_string()
    {
        return text;
    }
};

#endif
