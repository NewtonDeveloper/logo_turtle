#ifndef PRINTER_H
#define PRINTER_H

#define letter_size 16

class Printer
{
public:
	Printer(const int &max_x, const int &max_y);

	void print_letter(char letter, int y_pos, int x_pos);
	void print_string(const char* string, int string_size, int y_pos, int x_pos);
	void print_int(int value, int y_pos, int x_pos);
	void set_color(const int color);
	void clear_screen();
	void point(const int &x, const int &y);
	void line(int start_x, int start_y, int end_x, int end_y);
	
	int _color;
	const int _max_x, _max_y, _max_xy;
};


#endif
