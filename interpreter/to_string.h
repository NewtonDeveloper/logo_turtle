extern "C"{
#include <stdlib.h>
}
#include <sstream>

#ifndef TO_STRING_H
#define TO_STRING_H
// please note that extending namespace is actually undefined behaviour
// (because the implementation can extend this namespace)
// but in that case we are doing it intentionally.
namespace ext {
template <typename T>
std::string to_string(const T &value)
{
        std::ostringstream os;
        os << value;
        return os.str();
}

inline int stoi(const std::string &s, size_t *idx = 0, int base = 10)
{
        char *endptr = 0;
        int v = strtol(s.c_str(), &endptr, base);
        if (idx) {
                *idx = endptr - s.c_str();
        }
        return v;
}

}
#endif
