#include "lcd_lib/Open1768_LCD.h"
#include "LPC17XX.h"
#include "lcd_lib/LCD_ILI9325.h"
#include "lcd_lib/asciiLib.h"
#include "tp_lib/TP_Open1768.h"
#include "PIN_LPC17xx.h"

#include "string.h"
#include "Printer.h"
#include "turtle.h"

#include <exception>
#include <vector>
#include <string>
#include "interpreter/shared_ptr.h"

#include "interpreter/interpreter.h"

using namespace std;


#define RDR (1<<0) //Receiver Data Ready


void send_string(const string &string_to_send)
{
	const char* str = string_to_send.c_str();
	
	while(*str != '\0')
	{
		if(LPC_UART0->LSR & (1 << 5) )
		{
			LPC_UART0->THR = *str;
			++str;
		}
	}
}


char recive_char()
{
	while(!(LPC_UART0->LSR & RDR)); //wait until any data arrives in Rx FIFO
	return LPC_UART0->RBR;
}

string recive_line()
{
	string line;
	
	while(true)
	{
		char c = recive_char();
		
		if(c != '\n')
			line.push_back(c);
		else break;
	}
	
	return line;
}

void set_config() //TODO sprawdzic czy dziala
{
	PIN_Configure(0,2,1,0,0);
	PIN_Configure(0,3,1,0,0);
	
	 //konfig z labek
	LPC_UART0->LCR = 3 | (1<<7);
	LPC_UART0->DLL = 14;
	LPC_UART0->DLM = 0;
	LPC_UART0->FCR = 6;
	LPC_UART0->LCR = 3;

	//temp = lcdReadReg(OSCIL_ON); // nie wiadomo od czego to
}

// GLOBAL OBJECTS

Turtle turtle(0, LCD_MAX_X/2, LCD_MAX_Y/2);

//*****************


int main()
{
	shared_ptr<int> ptr(new int(10));
	string input( "(13-4)-(2+1)" );
	
	vector<Token> tokens = lex(input);
	Element* parsed = parse(tokens);
	int val = parsed->eval();
	
	Printer printer(LCD_MAX_X, LCD_MAX_Y);
	
	PIN_Configure(0,2,1,0,0);
	PIN_Configure(0,3,1,0,0);
	
	 //konfig z labek
	LPC_UART0->LCR = 3 | (1<<7);
	LPC_UART0->DLL = 14;
	LPC_UART0->DLM = 0;
	LPC_UART0->FCR = 6;
	LPC_UART0->LCR = 3;


	printer.clear_screen();
	
	printer.print_int(0, 17, 5);
	printer.print_int(printer._max_y, 15, 290);
	printer.print_int(printer._max_x, 240, 5);
	
	//arrow x
	printer.line(3, 3, 220, 3);
	printer.line(217, 1, 220, 3);
	printer.line(217, 5, 220, 3);
	printer.print_string("x", 1,215, 7);
	//arrow y
	printer.line(3, 3, 3, 270);
	printer.line(5, 267, 3, 270);
	printer.line(1, 267, 3, 270);
	printer.print_string("y", 1, 20, 262);
	//zaznacz zolwia
	turtle.forward(0);
	
	int iter = 0;
	while(true)
	{
		try {
		string line = recive_line();
		printer.print_string("                                                 ", 28, 20, 20);
		int length = 28;
		if((line.length()-1) < length)
			length = line.length()-1;
		printer.print_string(line.c_str(), length, 20, 20);
		printer.print_string("                                                 ", 23, 240, 40);

		tokens = lex(line);
		parsed = parse(tokens);
		parsed->eval();
		delete parsed;
		}
		catch(exception &e) {++iter; printer.print_int(iter, 240, 100); printer.print_string("blad: sproboj ze spacja", 23, 240, 40); }
	}
	
	printer.print_string("end", 3, 220, 35);
}
