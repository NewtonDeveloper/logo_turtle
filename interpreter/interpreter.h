#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <vector>
#include <string>

#include "Token.h"

struct Element {
    /*
       Funkcja eval zwraca wartość elementu (ewaluuje)
    */
    virtual int eval() const = 0;
    virtual ~Element(){}
};


vector<Token> lex(const string &input);
Element * parse(const vector<Token> &tokens);
 

/*
    Parsowanie. Przekształca sekwencję Tokenów w struktury obiektowe. Zaczynamy
    od utworzenia Elementu.
*/
struct Integer : Element {
    int value;
    explicit Integer(const int value) : value(value) {}
    int eval() const override  {return value; }
    virtual ~Integer(){}
};

/*
    W naszym problemie wszystkie operacje są binarne - dodajemy coś do czegoś.
*/
struct BinaryOperation : Element {
    enum Type { addition, subtraction, no_operation, nothing,
        left_command, right_command, forward_command , 
        back_command, penup_command, pendown_command,
        getxy_command, setxy_command,
        clearscreen_command, multiplication, divide} type;

    virtual ~BinaryOperation()
    {
    }

   /*
        Operacja ma element po lewej i po prawej stronie znaku operacji
    */
    Element *lhs = nullptr;
    Element *rhs = nullptr;

    int eval() const override;

};



#endif


